﻿using System;
using System.Threading.Tasks;

namespace OnSive.Meh.com.Test
{
    internal class Program
    {
        private const string API_KEY = "";

        private static async Task Main(string[] args)
        {
            //var a = await Meh.GetAsync();
            Meh.ApiKey = API_KEY;
            var b = await Meh.GetAsync();
            var c = await Meh.GetAsync(API_KEY);
            Console.ReadKey();
        }
    }
}