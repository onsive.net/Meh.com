[![NuGet Badge Version](https://img.shields.io/nuget/v/OnSive.Meh.com?style=for-the-badge)
![NuGet Badge Downloads](https://img.shields.io/nuget/dt/OnSive.Meh.com?style=for-the-badge)](https://www.nuget.org/packages/OnSive.Meh.com)

<br />
<div align="center">
  <img src="https://gitlab.com/onsive.net/Meh.com/-/raw/master/OnSive.Meh.com/Icon.png" alt="Logo" width="80" height="80">

  <h3 align="center">OnSive | Meh.com</h3>

  <p align="center">
    Small libary to get the data from <a href="https://meh.com/" title="Prosymbols">Meh.com</a>.
  <br />
  <a href="/issues/new">Report Bug</a>
    ·
  <a href="/issues/new">Request Feature</a>
  </p>
</div>

### Get

```
Install-Package OnSive.Meh.com
```

### Usage

Get you're Meh.com API key: [Meh.com/developers-developers-developers](https://meh.com/developers-developers-developers)

Single call:
```
var mehData = await Meh.GetAsync("{API Key}");
```

Multiple calls:
```
Meh.ApiKey = "{API Key}";

var mehData = await Meh.GetAsync();
...
var mehData = await Meh.GetAsync();
```

### Third Party

_Icon made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>_