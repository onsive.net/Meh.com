﻿using System;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OnSive.Meh.com.Models;

#if NET6_0

using OnSive.Core.Helper.Extensions;

#endif

[assembly: InternalsVisibleTo("OnSive.Meh.com.Test")]

namespace OnSive.Meh.com
{
    public static class Meh
    {
        public static string ApiKey { get; set; }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException">If no ApiKey was previously set</exception>
        /// <exception cref="HttpRequestException"></exception>
        public static async Task<MehData> GetAsync()
            => await GetAsync(ApiKey).ConfigureAwait(true);

        /// <summary>
        ///
        /// </summary>
        /// <param name="ApiKey"></param>
        /// <exception cref="HttpRequestException"></exception>
        /// <returns></returns>
        public static async Task<MehData> GetAsync(string ApiKey)
        {
#if NET6_0
            ApiKey.ThrowArgumentNullOrEmpty();
#else
            if (string.IsNullOrEmpty(ApiKey))
                throw new ArgumentNullException(nameof(ApiKey));
#endif

            using var Client = new HttpClient();
            var response = await Client.GetAsync($"https://api.meh.com/1/current.json?apikey={ApiKey}").ConfigureAwait(true);
            response.EnsureSuccessStatusCode();
            var content = await response.Content.ReadAsStringAsync().ConfigureAwait(true);
            return DeserializeData(content);
        }

        internal static MehData DeserializeData(string data)
            => JsonConvert.DeserializeObject<MehData>(data);
    }
}