﻿using Newtonsoft.Json;

namespace OnSive.Meh.com.Models
{
    public class MehAnswer
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }

        [JsonProperty("voteCount")]
        public int VoteCount { get; set; }
    }
}