﻿using System;
using Newtonsoft.Json;

namespace OnSive.Meh.com.Models
{
    public class MehLaunch
    {
        [JsonProperty("soldOutAt")]
        public DateTime? SoldOutAt { get; set; }
    }
}
