﻿using Newtonsoft.Json;

namespace OnSive.Meh.com.Models
{
    public class MehAttribute
    {
        [JsonProperty("key")]
        public string Key { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }
}
