﻿using Newtonsoft.Json;

namespace OnSive.Meh.com.Models
{
    public class MehStory
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("body")]
        public string Body { get; set; }
    }
}
