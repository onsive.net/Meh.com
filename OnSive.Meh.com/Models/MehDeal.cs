﻿using System;
using Newtonsoft.Json;

namespace OnSive.Meh.com.Models
{
    public class MehDeal
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("features")]
        public string Features { get; set; }

        [JsonProperty("items")]
        public MehItem[] Items { get; set; }

        [JsonProperty("launches")]
        public MehLaunch[] Launches { get; set; }

        [JsonProperty("photos")]
        public string[] Photos { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("specifications")]
        public string Specifications { get; set; }

        [JsonProperty("story")]
        public MehStory Story { get; set; }

        [JsonProperty("theme")]
        public MehTheme Theme { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("soldOutAt")]
        public DateTime? SoldOutAt { get; set; }

        [JsonProperty("topic")]
        public MehTopic Topic { get; set; }
    }
}