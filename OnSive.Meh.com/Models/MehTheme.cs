﻿using Newtonsoft.Json;

namespace OnSive.Meh.com.Models
{
    public class MehTheme
    {
        [JsonProperty("accentColor")]
        public string AccentColor { get; set; }

        [JsonProperty("backgroundColor")]
        public string BackgroundColor { get; set; }

        [JsonProperty("backgroundImage")]
        public string BackgroundImage { get; set; }

        [JsonProperty("foreground")]
        public string Foreground { get; set; }
    }
}
