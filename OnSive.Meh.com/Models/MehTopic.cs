﻿using System;
using Newtonsoft.Json;

namespace OnSive.Meh.com.Models
{
    public class MehTopic
    {
        [JsonProperty("commentCount")]
        public int CommentCount { get; set; }

        [JsonProperty("createdAt")]
        public DateTime? CreatedAt { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("replyCount")]
        public int ReplyCount { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("voteCount")]
        public int VoteCount { get; set; }
    }
}