﻿using System;
using Newtonsoft.Json;

namespace OnSive.Meh.com.Models
{
    public class MehPoll
    {
        [JsonProperty("answers")]
        public MehAnswer[] Answers { get; set; }

        [JsonProperty("id")]
        public string id { get; set; }

        [JsonProperty("startDate")]
        public DateTime? StartDate { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("topic")]
        public MehTopic Topic { get; set; }
    }
}
