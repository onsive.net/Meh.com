﻿using Newtonsoft.Json;

namespace OnSive.Meh.com.Models
{
    public class MehItem
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("attributes")]
        public MehAttribute[] Attributes { get; set; }

        [JsonProperty("condition")]
        public string Condition { get; set; }

        [JsonProperty("price")]
        public int Price { get; set; }

        [JsonProperty("photo")]
        public string Photo { get; set; }
    }
}