﻿using System;
using Newtonsoft.Json;

namespace OnSive.Meh.com.Models
{
    public class MehVideo
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("startDate")]
        public DateTime? StartDate { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("topic")]
        public MehTopic Topic { get; set; }
    }
}