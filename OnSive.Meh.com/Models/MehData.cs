﻿using Newtonsoft.Json;

namespace OnSive.Meh.com.Models
{
    public class MehData
    {
        [JsonProperty("deal")]
        public MehDeal Deal { get; set; }

        [JsonProperty("poll")]
        public MehPoll Poll { get; set; }

        [JsonProperty("video")]
        public MehVideo Video { get; set; }
    }
}
